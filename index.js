#!/usr/bin/env node

var exec = require('child_process').exec;

// function puts(error, stdout, stderr) {
//   if (error) console.error(error);
//   if (stdout) console.log(stdout);
//   if (stderr) console.warn(stderr);
// }

function airdrop() {
  console.log(`Cloning ${process.argv[2]} to directory ${process.argv[3]}...`);
  exec(`git clone ${process.argv[2]} ${process.argv[3]}`, function(err, res) {
    if (err) {
      throw err;
    } else {
      console.log("Deleting dropped repository's git history...");
      exec(`cd ${process.argv[3]}; rm -rf ./.git; rm .gitignore`, function(err, res) {
        if (err) {
          throw err;
        } else {
          console.log("Airdrop complete.");
        }
      });
    }
  });
}

airdrop();
