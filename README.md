# airdrop

A utility for dropping component git repositories into other git repositories,
then erasing their git history.

## Use

1. `npm install -g airdrop`
2. `airdrop GIT_URL DESTINATION_FOLDER`

At this point, `DESTINATION_FOLDER` will have only the contents of the
repository at `GIT_URL`, with git metadata removed.
